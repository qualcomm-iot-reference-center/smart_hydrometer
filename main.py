import cv2, imutils
from datetime import datetime
from tensorflow.keras.models import load_model
from tensorflow.keras.utils import CustomObjectScope
from tensorflow.keras.initializers import glorot_uniform
import numpy as np

vobj = cv2.VideoCapture("http://172.16.100.106/api/mjpegvideo.cgi?")

# ROIS 30 x 56
x_offset = -70
y_offset = 12

points = []
# (xinicial, yinicial), (xfinal, yfinal)
points.append(((255 + x_offset, 190 + y_offset), (285 + x_offset, 246 + y_offset)))
points.append(((311 + x_offset, 190 + y_offset), (341 + x_offset, 246 + y_offset)))
points.append(((365 + x_offset, 190 + y_offset), (395 + x_offset, 246 + y_offset)))
points.append(((420 + x_offset, 190 + y_offset), (450 + x_offset, 246 + y_offset)))
points.append(((473 + x_offset, 190 + y_offset), (503 + x_offset, 246 + y_offset)))
points.append(((526 + x_offset, 190 + y_offset), (556 + x_offset, 246 + y_offset)))


# Carrega o modelo
with CustomObjectScope({'GlorotUniform': glorot_uniform()}):
    model = load_model('hydrometer_model.h5')

    # Loop de execução
    while (1):
        status = False
        while status is not True:
            status, frame = vobj.read()
        status = False

        frame = imutils.rotate(frame, angle = -96.6544)

        rois_list = []
        for point in points:
            rois_list.append(frame[point[0][1]:point[1][1], point[0][0]:point[1][0]])

        prediction = model.predict(np.array(rois_list))
        classification = prediction.argmax(axis = 1)

        with open('classification.txt', 'w') as f:
            write_data = ';'.join([str(num) for num in classification])
            f.write(write_data)

        # for i, point in enumerate(points):
        #     cv2.putText(frame, str(classification[i]), (int((point[1][0] - point[0][0]) / 2 + point[0][0] - 10), point[0][1] - 10), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX, 1, (0, 0, 255), 2)
        #     cv2.rectangle(frame, point[0], point[1], (255,0,0), 1)
        
        # cv2.imshow("Output", frame)

        # key = cv2.waitKey(1) & 0xFF

        # if key == ord("q"):
        #     break

vobj.release()
cv2.destroyAllWindows()