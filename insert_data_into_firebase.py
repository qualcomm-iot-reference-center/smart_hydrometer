# -*- coding: utf-8 -*-
import firebase_admin
import requests
import datetime
import time

from firebase_admin import credentials, db
from datetime import datetime as dt

# Set credentials to firebase
cred = credentials.Certificate("/home/linaro/smart_hydrometer/firebase-sdk.json")

# Initialize an application
firebase_admin.initialize_app(
    cred, {"databaseURL": "https://hidrometroteste-fe09c.firebaseio.com/"}
)

numbers = 6  # number of caracters of the hidrometer
current_validated = 0
time.sleep(60)  # Initialize Dragonboard
time_list = [
    "09:00",
    "11:10",
    "13:00",
    "14:00",
    "15:00",
]
while True:
    try:
        # Set current time
        date_now = str(dt.now().date())
        time_now = dt.now().time()
        time_now = str(time_now.isoformat(timespec="minutes"))
        time_stamp = date_now + " " + time_now

        # Get predict data
        with open("/home/linaro/smart_hydrometer/classification.txt") as f:
            values = f.read().split(";")

        # Concatenate the prediction info (string)
        prediction_string = ""
        for i in range(0, numbers):
            values[i] = str(values[i])
            prediction_string = prediction_string + values[i]

        # Transform from str to int
        prediction_string = int(prediction_string)

        # Send data to firebase
        try:
            if time_now == "09:00":  # First time of the day send the date and time
                current_validated = prediction_string
                ref = db.reference("/")
                ref.update(
                    {date_now: {time_now: current_validated,},}
                )
            elif (
                time_now in time_list
            ):  # Just send the time because de date already exist
                if (current_validated < prediction_string) and ((current_validated - prediction_string) < 10000):
                    current_validated = prediction_string
                ref = db.reference(date_now)
                ref.update(
                    {time_now: current_validated,}
                )
        except Exception as ex:
            print("Exception: " + str(ex))


        # Send data to SQL Server Smart Campus
        try:
            # JSON for SQL Server of Smart Campus
            data = {"date": time_stamp, "prediction": current_validated}
            if time_now in time_list:
                requests.post(
                    "http://www3.facens.br/smartcampus//api/hidrometro/index.php",
                    json=data,
                )
                print(data)
        except HTTPError as http_err:
            print(f"HTTP error occurred: {http_err}")
        except Exception as err:
            print(f"Other error occurred: {err}")

        time.sleep(50)
    except Exception as ex:
        print("Exception: " + str(ex))


