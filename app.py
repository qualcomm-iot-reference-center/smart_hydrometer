from flask import Flask, render_template, jsonify

app = Flask(__name__)

@app.route('/')
def main_page():
    return render_template('index.html')

@app.route('/english')
def english():
    return render_template('english.html')

@app.route('/_update', methods = ['GET'])
def update():
    with open('classification.txt') as f:
        data = f.read().split(';')
    return jsonify(n1=data[0], n2=data[1], n3=data[2], n4=data[3], n5=data[4], n6=data[5])


if __name__ == '__main__':
    app.run(debug = True,   host='0.0.0.0', port=8000)