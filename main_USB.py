import cv2, imutils
import shlex, subprocess
from datetime import datetime
from tensorflow.keras.models import load_model
from tensorflow.keras.utils import CustomObjectScope
from tensorflow.keras.initializers import glorot_uniform
import numpy as np
import time
from datetime import datetime

# Function to execute a command in bash
def python_to_bash(comando_bash):
    args = shlex.split(comando_bash)
    comando = subprocess.Popen(args, stdout=subprocess.PIPE, shell=True)
    saida, erro = comando.communicate()
    comando = subprocess.check_output(args)
    return comando


lista_dispositivos = "v4l2-ctl --list-devices"
saida = python_to_bash(lista_dispositivos)
saida = saida.decode(encoding="UTF-8")

lista_saida = saida.split("\n")

for n_linha, texto in enumerate(lista_saida):
    if "usb" in texto:
        nome_camera = lista_saida[n_linha + 1].strip()
        video = int(nome_camera[-1])
print(video)

vobj = cv2.VideoCapture(video)
# ROIS 30 x 40
x_offset = -84
y_offset = -102


points = []
# (xinicial, yinicial), (xfinal, yfinal)
points.append(((255 + x_offset, 190 + y_offset), (285 + x_offset, 230 + y_offset)))
points.append(((291 + x_offset, 190 + y_offset), (321 + x_offset, 230 + y_offset)))
points.append(((330 + x_offset, 190 + y_offset), (360 + x_offset, 230 + y_offset)))
points.append(((367 + x_offset, 190 + y_offset), (397 + x_offset, 230 + y_offset)))
points.append(((405 + x_offset, 190 + y_offset), (435 + x_offset, 230 + y_offset)))
points.append(((443 + x_offset, 190 + y_offset), (473 + x_offset, 230 + y_offset)))


# (xinicial, yinicial), (xfinal, yfinal)
N1 = ((255 + x_offset), (190 + y_offset), (285 + x_offset), (230 + y_offset))
N2 = ((291 + x_offset), (190 + y_offset), (321 + x_offset), (230 + y_offset))
N3 = ((330 + x_offset), (190 + y_offset), (360 + x_offset), (230 + y_offset))
N4 = ((367 + x_offset), (190 + y_offset), (397 + x_offset), (230 + y_offset))
N5 = ((405 + x_offset), (190 + y_offset), (435 + x_offset), (230 + y_offset))
N6 = ((443 + x_offset), (190 + y_offset), (473 + x_offset), (230 + y_offset))

k = 0
start_time = datetime.now()
frame_counter = 30

# Carrega o modelo
with CustomObjectScope({"GlorotUniform": glorot_uniform()}):
    model = load_model("/home/linaro/smart_hydrometer/hydrometer_model_bin_all.h5")
    # Loop de execução
    while True:
        i = 0
        status, frame = vobj.read()
        if not status:
            vobj = cv2.VideoCapture(video)
            continue

        frame = imutils.rotate(frame, angle=95)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gaus = cv2.adaptiveThreshold(
            gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2
        )

        rois_list = []
        for point in points:
            rois_list.append(gaus[point[0][1] : point[1][1], point[0][0] : point[1][0]])
            rois_list[i] = rois_list[i].reshape((40, 30, 1))
            i = i + 1

        """
        # Get images to train
        time_now = datetime.now()
        get_images_list = []
        # ROIS das regiões de interesse
        get_images_list.append(gaus[N1[1] : N1[3], N1[0] : N1[2]])
        get_images_list.append(gaus[N2[1] : N2[3], N2[0] : N2[2]])
        get_images_list.append(gaus[N3[1] : N3[3], N3[0] : N3[2]])
        get_images_list.append(gaus[N4[1] : N4[3], N4[0] : N4[2]])
        get_images_list.append(gaus[N5[1] : N5[3], N5[0] : N5[2]])
        get_images_list.append(gaus[N6[1] : N6[3], N6[0] : N6[2]])

        if (time_now - start_time).seconds > 10:
            for m, img in enumerate(get_images_list):
                cv2.imwrite("/home/linaro/smart_hydrometer/images/n_{}_{}.jpeg".format(m, k), img)
                start_time = datetime.now()
                k += 1
        """
        

        if (frame_counter % 30) == 0:
            prediction = model.predict(np.array(rois_list))
            classification = prediction.argmax(axis=1)

            with open("/home/linaro/smart_hydrometer/classification.txt", "w") as f:
                write_data = ";".join([str(num) for num in classification])
                f.write(write_data)

        """
        for i, point in enumerate(points):
            cv2.putText(
                frame,
                str(classification[i]),
                (
                    int((point[1][0] - point[0][0]) / 2 + point[0][0] - 10),
                    point[0][1] - 10,
                ),
                cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
                1,
                (0, 0, 255),
                2,
            )
            cv2.rectangle(frame, point[0], point[1], (255, 0, 0), 1)

        cv2.imshow("Output_frame", frame)
        cv2.imshow("Output_gaus", gaus)
        """

        frame_counter += 1

        key = cv2.waitKey(1) & 0xFF

        if key == ord("q"):
            break

vobj.release()
cv2.destroyAllWindows()

