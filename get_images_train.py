import cv2
import imutils
import numpy as np
import shlex, subprocess
from datetime import datetime


def adjust_gamma(image, gamma):
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 200 for i in range(0, 256)]).astype(
        "uint8"
    )
    return cv2.LUT(image, table)

# Function to execute a command in bash
def python_to_bash(comando_bash):
    args = shlex.split(comando_bash)
    comando = subprocess.Popen(args, stdout=subprocess.PIPE, shell=True)
    saida, erro = comando.communicate()
    comando = subprocess.check_output(args)
    return comando


lista_dispositivos = "v4l2-ctl --list-devices"
saida = python_to_bash(lista_dispositivos)
saida = saida.decode(encoding="UTF-8")

lista_saida = saida.split("\n")

for n_linha, texto in enumerate(lista_saida):
    if "usb" in texto:
        nome_camera = lista_saida[n_linha + 1].strip()
        video = int(nome_camera[-1])
print (video)

vobj = cv2.VideoCapture(video)

# ROIS 30 x 24
x_offset = -17
y_offset = -37


# (xinicial, yinicial), (xfinal, yfinal)
N1 = ((255 + x_offset), (190 + y_offset), (285 + x_offset), (230 + y_offset))
N2 = ((287 + x_offset), (190 + y_offset), (317 + x_offset), (230 + y_offset))
N3 = ((319 + x_offset), (190 + y_offset), (349 + x_offset), (230 + y_offset))
N4 = ((351 + x_offset), (190 + y_offset), (381 + x_offset), (230 + y_offset))
N5 = ((383 + x_offset), (190 + y_offset), (413 + x_offset), (230 + y_offset))
N6 = ((415 + x_offset), (190 + y_offset), (445 + x_offset), (230 + y_offset))

# Adjust Gamma Values
gamma = (0.50, 0.75, 1.0)

i = 0
start_time = datetime.now()
status = False

while True:
    time_now = datetime.now()
    rois_list = []

    status, frame = vobj.read()

    if not status:
        vobj = cv2.VideoCapture(video)
        continue

    status = False

    frame = imutils.rotate(frame, angle=-1)

    # ROIS das regiões de interesse
    rois_list.append(frame[N1[1] : N1[3], N1[0] : N1[2]])
    rois_list.append(frame[N2[1] : N2[3], N2[0] : N2[2]])
    rois_list.append(frame[N3[1] : N3[3], N3[0] : N3[2]])
    rois_list.append(frame[N4[1] : N4[3], N4[0] : N4[2]])
    rois_list.append(frame[N5[1] : N5[3], N5[0] : N5[2]])
    rois_list.append(frame[N6[1] : N6[3], N6[0] : N6[2]])

    if (time_now - start_time).seconds > 60:
        print(time_now)
        for j, img in enumerate(rois_list):
            for g, gam in enumerate(gamma):
                auxgam = adjust_gamma(img, gam)
                cv2.imwrite(
                    "/media/linaro/294e874d-059d-4757-9973-e2750ee6b2af/hydrometer/images/n{}_{}_gamma_{}.jpeg".format(
                        j + 1, i, gam
                    ),
                    auxgam,
                )
        start_time = datetime.now()
        i += 1

    # cv2.imshow("Output", frame)
    # cv2.imshow("N1", rois_list[0])
    # cv2.imshow("N2", rois_list[1])
    # cv2.imshow("N3", rois_list[2])
    # cv2.imshow("N4", rois_list[3])
    # cv2.imshow("N5", rois_list[4])
    # cv2.imshow("N6", rois_list[5])

    key = cv2.waitKey(1) & 0xFF

    if key == ord("q"):
        break

vobj.release()
cv2.destroyAllWindows()
